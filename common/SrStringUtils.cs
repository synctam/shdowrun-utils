﻿namespace SrStringUtils
{
    using System.Text;
    using System.Text.RegularExpressions;

    public class SrStringUtils
    {
        /// <summary>
        /// 日本語処理関連汎用関数
        /// </summary>
        public class TJapaneseStringUtils
        {
            /// <summary>
            /// 日本語と日本語の間に指定した文字を挿入し、挿入後のテキストを返す。
            /// </summary>
            /// <param name="text">処理対象のテキスト</param>
            /// <param name="sp">挿入する文字</param>
            /// <returns>挿入後のテキスト</returns>
            public static string InsertSpace(string text, char sp)
            {
                StringBuilder buff = new StringBuilder();

                bool isJapaneseMode = false;
                char prevChar = '\0';
                foreach (char c in text)
                {
                    if (isJapaneseMode && IsNihongo(c))
                    {
                        //// prev:JP next:JP = スペースを挿入する。
                        buff.Append(sp);

                        buff.Append(c);
                        isJapaneseMode = true;
                    }
                    else if (!isJapaneseMode && IsNihongo(c))
                    {
                        //// prev:EN next:JP
                        buff.Append(c);
                        isJapaneseMode = true;
                    }
                    else if (isJapaneseMode && !IsNihongo(c))
                    {
                        //// prev:JP next:EN
                        if (c == ' ')
                        {
                            //// 日本語に次にある半角スペースは「EN SP」に変更する。
                            buff.Append('\u2002');
                            isJapaneseMode = true;
                        }
                        else
                        {
                            buff.Append(c);
                            isJapaneseMode = false;
                        }
                    }
                    else if (!isJapaneseMode && !IsNihongo(c))
                    {
                        //// prev:EN next:EN
                        buff.Append(c);
                        isJapaneseMode = false;
                    }

                    prevChar = c;
                }

                return buff.ToString();
            }

            /// <summary>
            /// 指定したテキストに日本語が含まれているかどうかを判定する。
            /// </summary>
            /// <param name="text">テキスト</param>
            /// <returns>true: 日本語が含まれる。</returns>
            public static bool ContainsJapanese(string text)
            {
                foreach (char c in text)
                {
                    if (IsNihongo(c))
                    {
                        return true;
                    }
                    else
                    {
                        // 日本語じゃないのでスキップ
                    }
                }

                return false;
            }

            /// <summary>
            /// 指定した文字が日本語化どうかを判定する。
            /// </summary>
            /// <param name="c">文字</param>
            /// <returns>true: 日本語</returns>
            public static bool IsNihongo(char c)
            {
                if ((c == '\r') || (c == '\n') || (c == '\t'))
                {
                    // 日本語じゃないのでスキップ
                }
                else
                {
                    if (IsHiragana(c) || IsKanji(c) || IsKatakana(c))
                    {
                        return true;
                    }
                    else
                    {
                        // 日本語じゃないのでスキップ
                    }
                }

                return false;
            }

            public static bool ContainsKatakanaOrHiragana(string text)
            {
                foreach (var c in text)
                {
                    if (TJapaneseStringUtils.IsHiragana(c) || TJapaneseStringUtils.IsKatakana(c))
                    {
                        return true;
                    }
                }

                return false;
            }

            /// <summary>
            /// 指定した文字が「カタカナ」かどうかを判定する。
            /// </summary>
            /// <param name="c">文字</param>
            /// <returns>カタカナの場合はtrue</returns>
            private static bool IsKatakana(char c)
            {
                return Regex.IsMatch(c.ToString(), @"^\p{IsKatakana}*$");
            }

            /// <summary>
            /// 指定した文字が「平仮名」かどうかを判定する。
            /// </summary>
            /// <param name="c">文字</param>
            /// <returns>平仮名の場合はtrue</returns>
            private static bool IsHiragana(char c)
            {
                return Regex.IsMatch(c.ToString(), @"^\p{IsHiragana}*$");
            }

            /// <summary>
            /// 指定した文字が「漢字」かどうかを判定する。
            /// </summary>
            /// <param name="c">文字</param>
            /// <returns>漢字の場合はtrue</returns>
            private static bool IsKanji(char c)
            {
                string matchingText =
                    @"[\p{IsCJKUnifiedIdeographs}" +
                    @"\p{IsCJKCompatibilityIdeographs}" +
                    @"\p{IsCJKUnifiedIdeographsExtensionA}]|" +
                    @"[\uD840-\uD869][\uDC00-\uDFFF]|\uD869[\uDC00-\uDEDF]";

                if (System.Text.RegularExpressions.Regex.IsMatch(c.ToString(), matchingText))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public class StringUtils
        {
            /// <summary>
            /// 制御文字をタグに変換したテキストを返す。
            /// </summary>
            /// <param name="text">テキスト</param>
            /// <returns>制御文字をタグに変換したテキスト</returns>
            public static string LineBreak2Tag(string text)
            {
                var buff = new StringBuilder(text);

                buff.Replace("\r\n", "CRLF");
                buff.Replace("\r", "CR");
                buff.Replace("\n", "LF");

                return buff.ToString();
            }

            /// <summary>
            /// タグを制御文字に変換したテキストを返す。
            /// </summary>
            /// <param name="text">テキスト</param>
            /// <returns>タグを制御文字に変換したテキスト</returns>
            public static string Tab2LineBreak(string text)
            {
                var buff = new StringBuilder(text);

                buff.Replace("CRLF", "\r\n");
                buff.Replace("CR", "\r");
                buff.Replace("LF", "\n");

                return buff.ToString();
            }

            /// <summary>
            /// 制御文字をタグに変換した文字列を返す。
            /// </summary>
            /// <param name="text">テキスト</param>
            /// <returns>制御文字をタグに変換した文字列</returns>
            public static string Tab2Tag(string text)
            {
                //// タブ文字(\t)は Google スプレッドシートで空白に変換されてしまうため、タグに変換する。
                var buff = new StringBuilder(text);

                buff.Replace("\t", "<TAB>");

                var result = buff.ToString();

                return result;
            }

            /// <summary>
            /// タグを制御文字に変換した文字列を返す。
            /// </summary>
            /// <param name="text">テキスト</param>
            /// <returns>タグを制御文字に変換した文字列</returns>
            public static string Tag2Tab(string text)
            {
                var buff = new StringBuilder(text);

                buff.Replace("<TAB>", "\t");

                return buff.ToString();
            }
        }
    }
}
