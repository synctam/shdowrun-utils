﻿namespace SruTransApply
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using LibPoUtils.Csv;
    using LibPoUtils.Data;
    using LibSruUtils.LangData;
    using LibSruUtils.PoUtils;
    using LibSruUtils.TransSheet;
    using S5mDebugTools;

    internal class Program
    {
        private static int Main(string[] args)
        {
            MakeCsv4CodeText();
            TDebugUtils.Pause();
            return 0;
        }

        private static void MakeCsv4CodeText()
        {
            var dataFileEn = new SruDataFile();
            SruTransSheetDao.LoadFromCsv(dataFileEn, @"data\csv\en\code_00003.txt.csv");

            var dataFileJp = new SruDataFile();
            SruTransSheetDao.LoadFromCsv(dataFileJp, @"data\csv\jp\code_00003.txt.csv");

            var translationWorker = new TranslationWorker();
            var margedDataFile = translationWorker.Merge(dataFileEn, dataFileJp);

            SruTransSheetDao.SaveToCsv(margedDataFile, @"data\csv\translated\code_00003.txt.csv");
        }

        private static void Apply001()
        {
            var dataFileBel = new SruDataFile();
            SruTransSheetDao.LoadFromCsv(dataFileBel, @"data\csv\berlin.csv");

            var dataFileDms = new SruDataFile();
            SruTransSheetDao.LoadFromCsv(dataFileDms, @"data\csv\deadmanswitch.csv");
            var dataFileDfe = new SruDataFile();
            SruTransSheetDao.LoadFromCsv(dataFileDfe, @"data\csv\DragonfallExtended.csv");
            var dataFileStl = new SruDataFile();
            SruTransSheetDao.LoadFromCsv(dataFileStl, @"data\csv\seattle.csv");

            var transApply = new TranslationWorker();
            transApply.Merge(dataFileBel, dataFileDms);
            transApply.Merge(dataFileBel, dataFileDfe);
            transApply.Merge(dataFileBel, dataFileStl);
            SruTransSheetDao.SaveToCsv(dataFileBel, @"data\csv\jp\berlin.csv");
            var pouDataFile = new PouDataFile("berlin");
            foreach (var dataEntry in dataFileBel.Items.Values)
            {
                var guidStr = Guid.NewGuid().ToString();
                var pouDataEntry = new PouDataEntry(guidStr, dataEntry.Source, dataEntry.Target);
                pouDataFile.AddEntry(pouDataEntry, false);
            }

            SruPoDao.SaveToPo(pouDataFile, @"data\po\berlin.po");
        }
    }
}
