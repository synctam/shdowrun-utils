﻿namespace SruTransApply
{
    using System;
    using LibSruUtils.LangData;

    internal class TranslationWorker
    {
        /// <summary>
        /// オリジナルに翻訳済みデータをマージしたデータファイルを返す。
        /// </summary>
        /// <param name="originalDataFile">オリジナルのデータファイル</param>
        /// <param name="translatedDataFile">翻訳済みデータファイル</param>
        /// <returns>マージ済みデータファイル</returns>
        public SruDataFile Merge(SruDataFile originalDataFile, SruDataFile translatedDataFile)
        {
            var newDataFile = new SruDataFile();
            foreach (var originalDataEntry in originalDataFile.Items.Values)
            {
                var newDataEntry = new SruDataEntry(originalDataEntry.Location, originalDataEntry.Source);

                var translatedDataEntry = translatedDataFile.GetEntry(originalDataEntry.Source);
                if (translatedDataEntry == null)
                {
                    //// 原文と翻訳文が同一の場合は、空文を代入。
                    newDataEntry.Target = string.Empty;
                }
                else
                {
                    if (newDataEntry.Source.Equals(translatedDataEntry.Target, StringComparison.OrdinalIgnoreCase))
                    {
                        //// 原文と翻訳文が同一の場合は、空文を代入。
                        newDataEntry.Target = string.Empty;
                    }
                    else
                    {
                        if (string.IsNullOrWhiteSpace(translatedDataEntry.Target))
                        {
                            //// 未翻訳の場合は、空文を転記。
                            newDataEntry.Target = string.Empty;
                        }
                        else
                        {
                            if (SrStringUtils.SrStringUtils.TJapaneseStringUtils.ContainsJapanese(translatedDataEntry.Target))
                            {
                                //// 翻訳文に日本語が含まれる場合
                                newDataEntry.Target = translatedDataEntry.Target;
                            }
                            else
                            {
                                //// 翻訳文に日本語が含まれない場合は、空文を転記。
                                newDataEntry.Target = string.Empty;
                            }
                        }
                    }
                }

                newDataFile.AddEntry(newDataEntry);
            }

            return newDataFile;
        }
    }
}
