namespace SruDiffer
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using LibPoUtils.Csv;
    using LibSruUtils.CsvUtils;
    using LibSruUtils.LangData;
    using LibSruUtils.PoUtils;
    using LibSruUtils.TransSheet;
    using S5mDebugTools;

    public class Program
    {
        private static int Main(string[] args)
        {
            MakeDiffCsv();

            TDebugUtils.Pause();
            return 0;
        }

        private static void MakeDiffCsv()
        {
            var oldFile = new SruDataFile();
            SruTransSheetDao.LoadFromCsv(oldFile, @"data\10.old\Dragonfall Extended.csv");

            var newFile = new SruDataFile();
            SruTransSheetDao.LoadFromCsv(newFile, @"data\11.new\Dragonfall Extended.csv");

            var diff = new SruDiff();
            diff.Diff(oldFile, newFile);

            SruTransSheetDao.SaveToCsv(diff, @"diffed.csv");
        }
    }
}
