namespace LibSruUtils.TransSheet
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using CsvHelper;
    using LibSruUtils.LangData;

    public class SruTransSheetDao
    {
        public static void SaveToCsv(SruDataFile dataFile, string path, Encoding enc = null)
        {
            if (enc == null)
            {
                enc = Encoding.UTF8;
            }

            using (var sw = new StreamWriter(path, false, enc))
            using (var writer = new CsvWriter(sw, CultureInfo.InvariantCulture))
            {
                writer.Configuration.RegisterClassMap<CsvMapper>();

                writer.WriteHeader<CsvRecord>();
                writer.NextRecord();

                //// 追加分のCSVファイルを作成する。
                foreach (var dataEntry in dataFile.Items.Values)
                {
                    var csvRecord = new CsvRecord();
                    csvRecord.Location = dataEntry.Location;
                    csvRecord.Source = dataEntry.Source;
                    csvRecord.Target = dataEntry.Target;

                    writer.WriteRecord(csvRecord);
                    writer.NextRecord();
                }
            }
        }

        public static void SaveToCsv(SruDiff dataDiff, string path, Encoding enc = null)
        {
            if (enc == null)
            {
                enc = Encoding.UTF8;
            }

            using (var sw = new StreamWriter(path, false, enc))
            using (var writer = new CsvWriter(sw, CultureInfo.InvariantCulture))
            {
                writer.Configuration.RegisterClassMap<CsvMapper>();

                writer.WriteHeader<CsvRecord>();
                writer.NextRecord();

                //// 追加分のCSVファイルを作成する。
                foreach (var diffEntry in dataDiff.NewItems.Values)
                {
                    var csvRecord = new CsvRecord();
                    csvRecord.Location = diffEntry.Location;
                    csvRecord.Source = diffEntry.Source;
                    csvRecord.Target = diffEntry.Target;

                    writer.WriteRecord(csvRecord);
                    writer.NextRecord();
                }
            }
        }

        public static void LoadFromCsv(SruDataFile dataFile, string path, Encoding enc = null)
        {
            if (enc == null)
            {
                enc = Encoding.UTF8;
            }

            using (var reader = new StreamReader(path, enc))
            {
                using (var csv = new CsvReader(reader, CultureInfo.InvariantCulture))
                {
                    csv.Configuration.Delimiter = ",";
                    csv.Configuration.HasHeaderRecord = true;
                    csv.Configuration.RegisterClassMap<CsvMapper>();

                    var records = csv.GetRecords<CsvRecord>();

                    string currentLocation = string.Empty;
                    foreach (var record in records)
                    {
                        var entry = new SruDataEntry(record.Location, record.Source);
                        if (!record.Source.Equals(record.Target))
                        {
                            entry.Target = record.Target;
                        }

                        dataFile.AddEntry(entry);
                    }
                }
            }
        }

        /// <summary>
        /// レコード：CSV書き込み時はこのクラスの定義順に出力される。
        /// </summary>
        public class CsvRecord
        {
            public string Location { get; set; } = string.Empty;

            public string Source { get; set; } = string.Empty;

            public string Target { get; set; } = string.Empty;

            ////public string MT { get; set; } = string.Empty;
        }

        /// <summary>
        /// 格納ルール ：マッピングルール(一行目を列名とした場合は列名で定義することができる。)
        /// </summary>
        public class CsvMapper : CsvHelper.Configuration.ClassMap<CsvRecord>
        {
            public CsvMapper()
            {
                // 出力時の列の順番は指定した順となる。
                this.Map(x => x.Location).Name("location");
                this.Map(x => x.Source).Name("source");
                this.Map(x => x.Target).Name("target");
                ////this.Map(x => x.MT).Name("Google翻訳");
            }
        }
    }
}
