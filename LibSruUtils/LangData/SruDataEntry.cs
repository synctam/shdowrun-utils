namespace LibSruUtils.LangData
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class SruDataEntry
    {
        public SruDataEntry(string location, string source)
        {
            this.Location = location;
            this.Source = source;
        }

        public string Location { get; } = string.Empty;

        public string Source { get; } = string.Empty;

        public string Target { get; set; } = string.Empty;
    }
}
