namespace LibSruUtils.LangData
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class SruDataFile
    {
        /// <summary>
        /// データエントリーの辞書
        /// キーは、原文のテキスト
        /// </summary>
        public Dictionary<string, SruDataEntry> Items { get; } =
            new Dictionary<string, SruDataEntry>(StringComparer.OrdinalIgnoreCase);

        public SruDataEntry GetEntry(string source)
        {
            if (this.Items.ContainsKey(source))
            {
                return this.Items[source];
            }
            else
            {
                return null;
            }
        }

        public void AddEntry(SruDataEntry entry)
        {
            if (this.Items.ContainsKey(entry.Source))
            {
                //// 重複は無視する。
                //// Console.WriteLine($"Duplicate key({entry.Source})");
                //// throw new Exception($"Duplicate key({entry.Source})");
            }
            else
            {
                this.Items.Add(entry.Source, entry);
            }
        }
    }
}
