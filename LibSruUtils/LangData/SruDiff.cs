namespace LibSruUtils.LangData
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class SruDiff
    {
        public Dictionary<string, SruDataEntry> NewItems { get; } =
            new Dictionary<string, SruDataEntry>(StringComparer.OrdinalIgnoreCase);

        public Dictionary<string, SruDataEntry> DeletedItems { get; } =
            new Dictionary<string, SruDataEntry>(StringComparer.OrdinalIgnoreCase);

        public void Diff(SruDataFile oldFile, SruDataFile newFile)
        {
            //// 削除項目の抽出
            foreach (var oldEntry in oldFile.Items.Values)
            {
                var newEntry = newFile.GetEntry(oldEntry.Source);
                if (newEntry == null)
                {
                    //// 削除：old に有って new に無い。
                    this.DeletedItems.Add(oldEntry.Source, oldEntry);
                }
                else
                {
                    //// 同一
                }
            }

            //// 追加項目の抽出
            foreach (var newEntry in newFile.Items.Values)
            {
                var oldEntry = oldFile.GetEntry(newEntry.Source);
                if (oldEntry == null)
                {
                    //// 追加：new に有って old に無い
                    this.NewItems.Add(newEntry.Source, newEntry);
                }
                else
                {
                    //// 同一
                }
            }
        }
    }
}
