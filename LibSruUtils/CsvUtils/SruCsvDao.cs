namespace LibSruUtils.CsvUtils
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using LibPoUtils.Csv;

    public class SruCsvDao
    {
        public static void LoadFromCsv(PouDataFile dataFile, string path)
        {
            PouCsvDao.LoadFromCsv(dataFile, path, true);
        }
    }
}
