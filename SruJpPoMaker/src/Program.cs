namespace SruJpPoMaker
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using LibPoUtils.Csv;
    using LibSruUtils.CsvUtils;
    using LibSruUtils.PoUtils;
    using S5mDebugTools;

    public class Program
    {
        private static int Main(string[] args)
        {
            Csv2Po();
            Csv2PoBerlin();

            TDebugUtils.Pause();
            return 0;
        }

        private static void Csv2PoBerlin()
        {
            var dataFile = new PouDataFile("Default");
            SruCsvDao.LoadFromCsv(dataFile, @"..\..\..\data\current\csv\berlin.csv");

            SruPoDao.SaveToPo(dataFile, @"..\..\..\data\current\po\berlin.po");
        }

        private static void Csv2Po()
        {
            var dataFile = new PouDataFile("Default");
            SruCsvDao.LoadFromCsv(dataFile, @"..\..\..\data\current\csv\0Shadowrun(N).csv");
            ////SruCsvDao.LoadFromCsv(dataFile, @"..\..\..\data\current\csv\0Shadowrun(D).csv");
            SruCsvDao.LoadFromCsv(dataFile, @"..\..\..\data\current\csv\0Shadowrun(Other).csv");
            SruCsvDao.LoadFromCsv(dataFile, @"..\..\..\data\current\csv\0Shadowrun(Response).csv");

            SruPoDao.SaveToPo(dataFile, @"..\..\..\data\current\po\DragonfallExtended.po");
        }
    }
}
